"""
A great example handle module which acts as a translation layer from AWS to the application logic.
"""

import json
import logging

from os import path

import app.my_resource as resource

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.DEBUG)


def get(event, _context):
    """
    Extracts the ID from the received event,
    fetches an entity by ID from my_resource
    and builds a response object
    """
    LOGGER.debug("Event: %s", json.dumps(event, indent=4))

    entity = resource.get(event["pathParameters"]["id"])

    return {
        "statusCode": 200,
        "headers": {
            "X-My-Custom-Header": "CUSTOOOOOOOOOM!"
        },
        "body": json.dumps(entity)
    }


def create(event, _context):
    """
    Passes the received to the my_resource.create function
    and then builds a response object containing the new entity.
    """
    LOGGER.debug("Event: %s", json.dumps(event, indent=4))

    body = json.loads(event["body"])
    entity = resource.create(body["name"])

    return {
        "statusCode": 200,
        "headers": {
            "Location": path.join(event["requestContext"]["path"], str(entity["id"])),
            "X-My-Custom-Header": "CUSTOOOOOOOOOM!"
        },
        "body": json.dumps(entity)
    }
