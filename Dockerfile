FROM lambci/lambda:build-python3.6

WORKDIR /function

RUN \
  curl -sL https://rpm.nodesource.com/setup_10.x | bash - &&\
  yum install --assumeyes nodejs &&\
  npm install --global serverless

RUN \
  pip install --upgrade pip &&\
  pip install --upgrade pipenv &&\
  pipenv --python 3.6

RUN \
  echo 'run() ( cd /function && ./docker-entrypoint.sh $@; )' >> ~/.bashrc &&\
  echo 'complete -W "deploy init lint test" run' >> ~/.bashrc

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["init", "bash"]