# pylint: disable=missing-docstring

import unittest

from app import my_resource as resource


class TestMyResource(unittest.TestCase):
    def test_get(self):
        entity = resource.get(42)
        expected = {"id": 42, "name": "Master of 42"}

        self.assertEqual(entity, expected)

    def test_create(self):
        entity = resource.create("my name")

        self.assertEqual(entity["name"], "my name")
        self.assertIsInstance(entity["id"], int)
        self.assertTrue(entity["id"] > 0)
