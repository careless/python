"""
A module to simulate some kind of resource which can be made accessible via a REST API.
"""

import random


def get(entity_id):
    """
    Returns the entity identified by the given ID.

    @param entity_id: an integer ID
    @return a dictionary with `id` and `name`
    """
    name = "Master of {}".format(entity_id)

    return {
        "id": entity_id,
        "name": name
    }


def create(name):
    """
    "Creates" a new entity with the given name and assigns it an ID.

    @param name: the name of the entity, must be a string
    @return a dictionary with `id` and `name`
    """
    entity_id = random.randint(0, 1000000)

    return {
        "id": entity_id,
        "name": name
    }
