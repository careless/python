# Python Function

Based on the [docker-lambda][docker-lambda] image for Python3.6 this template makes developing Python lambdas a breeze. It comes with the following libraries preinstalled:
- [`boto3`][python-aws-sdk] the Python AWS SDK
- [`pylint`][python-pylint] for linting

For testing the [`unittest`][python-unittest] library is being used.

Instead of using the rather brittle `requirements.txt` approach for `pip` it instead makes use of [`pipenv`][python-pipenv] which uses the new and shiny [`Pipfile`][python-pipfile]. In case you need an additional dependency you can simply run `pipenv install <dependency>` **in the container** (for dev dependencies add `--dev`); `pipenv` will automatically update the `Pipfile`.

For deployment the function has the [`serverless-python-requirements`][serverless-python-requirements] plugin installed. This automatically takes care of packaging up your dependencies alongside the actual lambda when deploying. No additional intaction from you required.

## `make`

*[For a rough overview take a look at the main README.](/README.md)*

Let's break down what the `Makefile` for Python does exactly.

### `make init`

__Depends on:__  `Pipfile` and `env/dev` (if missing, get's generated with empty default variables for AWS credentials)

#### Tasks
- `docker-compose build`
- `docker-compose run --rm function init` - runs `pipenv install --dev` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L18-L23))
- `touch Pipfile.lock .installed` - `.installed` is acts as a "flat" that the initialization happened and is being ignored by git

### `make shell`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function bash` - runs `SHELL=/bin/bash pipenv shell` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L57-L63))

### `make lint`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function lint` - runs `pipenv run python -m pylint handler.py app` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L50-L55))

### `make test`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function test` - runs `pipenv run python -m unittest` in the container ([docker-entrypoint.sh](docker-entrypoint.sh#L43-L48))

### `make deploy`

__Depends on:__ init

#### Tasks
- `docker-compose run --rm function test` ([docker-entrypoint.sh](docker-entrypoint.sh#L25-L41))
  - ensures that `ENV` is set (and not `local`)
  - installs the `serverless` plugins, if necessary
  - runs `serverless deploy --stage "$ENV" --verbose`


[docker-lambda]: https://github.com/lambci/docker-lambda
[python-aws-sdk]: https://aws.amazon.com/sdk-for-python/
[python-pylint]: https://www.pylint.org/
[python-pipenv]: https://docs.pipenv.org/
[python-pipfile]: https://github.com/pypa/pipfile
[python-unittest]: https://docs.python.org/3.6/library/unittest.html
[serverless-python-requirements]: https://github.com/UnitedIncome/serverless-python-requirements